Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2017-2023, Dirk Farin
 2017-2020, Struktur AG
License: Expat and/or LGPL

Files: fuzzing/* fuzzing/corpus/* gdk-pixbuf/* gnome/* go/*
Copyright:
 2017 struktur AG, Dirk Farin <farin@struktur.de>
 2017 struktur AG, Joachim Bauch <bauch@struktur.de>
License: LGPL-3+

Files: aclocal.m4
Copyright: 1996-2021, Free Software Foundation, Inc.
License: (FSFULLR and/or GPL-2+) with Autoconf-data exception

Files: autogen.sh
Copyright: 2017, 2018, struktur AG, Joachim Bauch <bauch@struktur.de>
License: GPL-3+

Files: compile
 depcomp
 missing
Copyright: 1996-2021, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: configure
Copyright: 1992-1996, 1998-2017, 2020, 2021, Free Software Foundation
License: FSFUL

Files: debian/*
Copyright: 2018 Joachim Bauch <bauch@struktur.de>
License: GPL-3+

Files: examples/*
Copyright: no-info-found
License: Expat

Files: examples/benchmark.cc
 examples/benchmark.h
Copyright: 2022, 2023, Dirk Farin <dirk.farin@gmail.com>
License: LGPL-3+

Files: examples/encoder.cc
 examples/encoder.h
 examples/encoder_jpeg.cc
 examples/encoder_jpeg.h
 examples/encoder_png.cc
 examples/encoder_png.h
 examples/heif_convert.cc
Copyright: 2017-2019, struktur AG, Joachim Bauch <bauch@struktur.de>
License: Expat

Files: examples/encoder_y4m.cc
 examples/encoder_y4m.h
 examples/heif-test.go
 examples/heif_enc.cc
 examples/heif_info.cc
 examples/heif_test.cc
 examples/heif_thumbnailer.cc
 examples/test_c_api.c
Copyright: 2017-2019, struktur AG, Dirk Farin <farin@struktur.de>
License: Expat

Files: extra/*
Copyright: 1987, 1993, 1994, 1996, The Regents of the University of California.
License: BSD-4-Clause-UC

Files: gdk-pixbuf/pixbufloader-heif.c
Copyright: 2019, Oliver Giles <ohw.giles@gmail.com>
License: LGPL-3+

Files: go/heif/*
Copyright: 2018, struktur AG, Dirk Farin <farin@struktur.de>
License: GPL-3+

Files: go/heif/heif_test.go
Copyright: 2017, 2018, struktur AG, Joachim Bauch <bauch@struktur.de>
License: GPL-3+

Files: install-sh
Copyright: 1994, X Consortium
License: X11

Files: libheif/*
Copyright: 2017-2020, struktur AG, Dirk Farin <farin@struktur.de>
License: LGPL-3+

Files: libheif/box_fuzzer.cc
 libheif/color_conversion_fuzzer.cc
 libheif/encoder_fuzzer.cc
 libheif/file_fuzzer.cc
Copyright: 2017-2019, struktur AG, Joachim Bauch <bauch@struktur.de>
License: LGPL-3+

Files: libheif/common_utils.cc
 libheif/common_utils.h
 libheif/exif.cc
 libheif/exif.h
 libheif/heif_init.cc
 libheif/heif_init.h
 libheif/metadata_compression.cc
 libheif/metadata_compression.h
 libheif/plugins_unix.cc
 libheif/plugins_unix.h
 libheif/plugins_windows.cc
 libheif/plugins_windows.h
Copyright: 2022, 2023, Dirk Farin <dirk.farin@gmail.com>
License: LGPL-3+

Files: libheif/plugins/heif_encoder_svt.cc
 libheif/plugins/heif_encoder_svt.h
Copyright: 2022, 2023, Dirk Farin <dirk.farin@gmail.com>
License: LGPL-3+

Files: ltmain.sh
Copyright: 1996-2015, Free Software Foundation, Inc.
License: (GPL-2+ and/or GPL-3+) with Libtool exception

Files: m4/*
Copyright: 2004, 2005, 2007-2009, 2011-2015, Free Software
License: FSFULLR

Files: m4/ax_cxx_compile_stdcxx_11.m4
Copyright: 2014, Alexey Sokolov <sokolov@google.com>
 2013, Roy Stogner <roystgnr@ices.utexas.edu>
 2012, Zack Weinberg <zackw@panix.com>
 2008, Benjamin Kosnik <bkoz@redhat.com>
License: FSFAP

Files: m4/libtool.m4
Copyright: 1996-2001, 2003-2015, Free Software Foundation, Inc.
License: (FSFULLR and/or GPL-2) with Libtool exception

Files: m4/ltversion.m4
 m4/visibility.m4
Copyright: 2004, 2005, 2011-2015, Free Software Foundation, Inc.
License: FSFULLR

Files: pre.js
Copyright: 2017, struktur AG, http://www.struktur.de, opensource@struktur.de
License: LGPL-3+

Files: scripts/*
Copyright: 2017, 2018, struktur AG, Joachim Bauch <bauch@struktur.de>
License: GPL-3+

Files: scripts/check-gofmt.sh
Copyright: 2018, struktur AG, Leon Klingele <leon@struktur.de>
License: GPL-3+

Files: scripts/cpplint.py
Copyright: 2009, Google Inc.
License: BSD-3-clause

Files: scripts/test-javascript.js
Copyright: 2017, struktur AG, http://www.struktur.de, opensource@struktur.de
License: LGPL-3+

Files: tests/*
Copyright: 2017-2019, struktur AG, Dirk Farin <farin@struktur.de>
License: Expat

Files: tests/catch.hpp
Copyright: 2022, Two Blue Cubes Ltd.
License: BSL-1.0

Files: tests/test-race.go
Copyright: 2017-2019, struktur AG, Joachim Bauch <bauch@struktur.de>
License: Expat

Files: Makefile.in extra/Makefile.in fuzzing/corpus/clusterfuzz-testcase-minimized-file-fuzzer-5651556035198976.heic fuzzing/corpus/clusterfuzz-testcase-minimized-file-fuzzer-5720856641142784.heic fuzzing/corpus/clusterfuzz-testcase-minimized-file-fuzzer-6045213633282048.heic fuzzing/corpus/colors-no-alpha.heic fuzzing/corpus/colors-with-alpha.heic fuzzing/corpus/github_138.heic libheif/Makefile.in
Copyright:
 2017 struktur AG, Dirk Farin <farin@struktur.de>
 2017 struktur AG, Joachim Bauch <bauch@struktur.de>
License: LGPL-3+

Files: examples/Makefile.in examples/demo.html examples/example.avif examples/example.heic examples/heif-convert.1 examples/heif-enc.1 examples/heif-info.1 examples/heif-thumbnailer.1
Copyright:
 2017 struktur AG, Joachim Bauch <bauch@struktur.de>
 2017 struktur AG, Dirk Farin <farin@struktur.de>
License: MIT

Files: scripts/Makefile.in
Copyright:
 2018 Joachim Bauch <bauch@struktur.de>
License: GPL-3+

Files: tests/Makefile.in
Copyright:
 2019 struktur AG, Dirk Farin <farin@struktur.de>
License: MIT
